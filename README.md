# OpenML dataset: gina_prior

https://www.openml.org/d/1042

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

**Note: derived from MNIST?**

Datasets from the Agnostic Learning vs. Prior Knowledge Challenge (http://www.agnostic.inf.ethz.ch)

Dataset from: http://www.agnostic.inf.ethz.ch/datasets.php

Modified by TunedIT (converted to ARFF format)


GINA is digit recognition database

The task of GINA is handwritten digit recognition. For the "prior knowledge track" we chose the problem of separating one-digit odd numbers from one-digit even numbers. The original pixel map representation is provided. This is a two class classification problem with sparse continuous input variables, in which each class is composed of several clusters. It is a problem with heterogeneous classes.

Data type: non-sparse
Number of features: 784
Number of examples and check-sum:
Pos_ex Neg_ex Tot_ex Check_sum
Train  1550  1603  3153 82735983.00
Valid   155   160   315 8243382.00

This dataset contains samples from both training and validation datasets.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1042) of an [OpenML dataset](https://www.openml.org/d/1042). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1042/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1042/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1042/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

